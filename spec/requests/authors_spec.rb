require 'rails_helper'

describe 'Author API', type: :request do
  describe 'GET /author' do
    it 'returns all author' do
      get '/authors'
      expect(response).to have_http_status(:ok)
      expect(response.body.size).to eq(2)
    end
  end
  describe 'POST /authors' do
    it 'create a new author' do
      expect {
        post '/authors', params: { author: { name: 'john doe' } }
      }.to change{Author.count}.from(0).to(1)
      expect(response).to have_http_status(:created)
      expect(response.body).to not_eq(nil)
    end
  end
end