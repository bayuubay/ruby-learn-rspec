require 'rails_helper'

describe 'Books API', type: :request do
  describe 'GET /books' do
    let!(:author){FactoryBot.create(:author, name: 'john doe') }
    it 'returns all books' do
      FactoryBot.create(:book, title: 'programming', author_id: author.id, description: 'all about programming')
      get '/books'
      expect(response).to have_http_status(:ok)
      # expect(response.body.size).to eq(1)
    end
  end
  describe 'POST /books' do
    let!(:author){FactoryBot.create(:author, name: 'john doe') }
    it 'create a new book' do
      expect {
        post '/books', params: { book: { title: 'programming', author_id: author.id, description: 'all about programming'}}
      }.to change{ Book.count }.from(0).to(1)
      expect(response).to have_http_status(:created)
    end
  end

  describe 'DELETE /books/:id' do
    let!(:author){ FactoryBot.create(:author, name: 'john doe') }
    let!(:book) { FactoryBot.create(:book, title: 'programming', author_id: author.id, description: 'all about programming') }
    it 'create a new book' do
      delete '/books/:id', params: { id: book.id }
      expect(response).to have_http_status(:no_content)
    end
  end
end