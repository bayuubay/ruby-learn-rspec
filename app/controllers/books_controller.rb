class BooksController < ApplicationController
  # before_action :book_params
  def index
    @books = Book.all
    return unless @books

    render json: @books, status: :ok
  end

  def new
  end

  def show
    
  end


  def create
    @book = Book.create(book_params)
    if @book.save
      render json: {book: @book, msg: 'success' }, status: :created
    else
      render json: { book: @book.errors }
    end
  end

  def update
  end

  def destroy
  end

  private

  def book_params
    params.require(:book).permit(:title, :author_id, :description)
  end

end
