class AuthorsController < ApplicationController
  # before_action :author_params
  def index
    @authors = Author.all
    return unless @authors

    render json: @authors, status: :ok
  end

  def new
  end

  def show
    
  end


  def create
    @author = Author.create(author_params)
    if @author.save
      render json: {author: @author, msg: 'success' }, status: :created
    else
      render json: { author: @author.errors }
    end
  end

  def update
  end

  def destroy
  end

  private

  def author_params
    params.require(:author).permit(:name)
  end

end
